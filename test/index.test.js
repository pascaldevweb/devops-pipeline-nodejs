const request = require('supertest');
const express = require('express');
const expect = require('chai').expect;

const app = require('../index');

describe('GET /', function() {
  it('responds with Hello, World!', function(done) {
    request(app)
      .get('/')
      .expect('Hello, World!', done);
  });
});