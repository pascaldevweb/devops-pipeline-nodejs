const port = 3000;
const app = require('./index');

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
  });